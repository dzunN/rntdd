import React from 'react';
import renderer, {act, create} from 'react-test-renderer';
import Home from '../src/screens/Home';

const tree = create(<Home />);
// Snapshot test
test('Home renders correctly', () => {
  const tree1 = renderer.create(<Home />).toJSON();
  expect(tree1).toMatchSnapshot();
});

// Test State changes
test('Button Sapa Pressed', () => {
  // Create a new instance of the component
  const button = tree.root.findByProps({testID: 'btn-sapa'}).props;

  // Simulate a click on the button
  act(() => button.onPress());

  // Check if the text has changed
  const text = tree.root.findByProps({testID: 'text-sapa'}).props;

  expect(text.children).toEqual('Halo Hindun');
});
