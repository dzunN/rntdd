import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import {loginUser} from '../src/api';
import {resLogin} from './dummy';

describe('Testing Api', () => {
  it('Api Login', async () => {
    let mock = new MockAdapter(axios);
    const loginBody = {
      email: 'omewa@email.com',
      password: 'omaewa2022',
    };
    mock
      .onPost('http://code.aldipee.com/api/v1/auth/login')
      .reply(200, resLogin);

    // //act
    let res = await loginUser(loginBody);

    expect(res.data).toEqual(resLogin);
    expect(res.status).toEqual(200);
  });
});
