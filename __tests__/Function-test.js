import {Addition, Subtraction, Multiplication} from '../src/utils/Math';

// Function test

test('Addition', () => {
  expect(Addition(1, 2)).toBe(3);
});

test('Subtraction', () => {
  expect(Subtraction(1, 2)).toBe(-1);
});

test('Multiplication', () => {
  expect(Multiplication(1, 2)).toBe(2);
});

test('Multiplication Wrong', () => {
  expect(Multiplication(1, 2)).toBe(2);
});
