export const resLogin = {
  user: {
    role: 'user',
    isEmailVerified: true,
    email: 'omewa@email.com',
    name: 'Omewa Chan',
    id: '6242f19b2c4ebe0f1d319442',
  },
  tokens: {
    access: {
      token:
        'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiI2MjQyZjE5YjJjNGViZTBmMWQzMTk0NDIiLCJpYXQiOjE2NDk5MzU1ODYsImV4cCI6MTY0OTkzNzM4NiwidHlwZSI6ImFjY2VzcyJ9.ZbhTcPYdBTFOkyTw7HWRWbBSO9t8c3qKVKfI8UtsBw0',
      expires: '2022-04-14T11:56:26.575Z',
    },
    refresh: {
      token:
        'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiI2MjQyZjE5YjJjNGViZTBmMWQzMTk0NDIiLCJpYXQiOjE2NDk5MzU1ODYsImV4cCI6MTY1MjUyNzU4NiwidHlwZSI6InJlZnJlc2gifQ.5XKgOil2cEU-R4X7h_xpZKfpqm-H2ug3TSOt565xLso',
      expires: '2022-05-14T11:26:26.576Z',
    },
  },
};
