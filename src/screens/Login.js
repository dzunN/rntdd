import React, {useState} from 'react';
import {ScrollView, StyleSheet, Text, View} from 'react-native';
import colors from '../assets/theme/colors';
import {CustomButton, CustomInput} from '../components';
import axios from 'axios';

export default function Login({navigation}) {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [loading, setLoading] = useState(false);

  // Heandle login
  const handleLogin = async () => {
    setLoading(true);
    try {
      const response = await axios.post(
        'http://code.aldipee.com/api/v1/auth/login',
        {
          email,
          password,
        },
      );
      console.log(response.data);
      setLoading(false);
      navigation.replace('Home');
    } catch (error) {
      console.log(error);
      setLoading(false);
    }
  };

  return (
    <ScrollView style={styles.scroll}>
      <View style={styles.container}>
        <View>
          <Text style={styles.title}>Please Login Firts</Text>
          <View style={styles.form}>
            <CustomInput
              label="Email"
              iconPosition="right"
              placeholder="Enter Email"
              value={email || null}
              onChangeText={value => {
                setEmail(value);
              }}
            />
            <CustomInput
              label="Password"
              iconPosition="right"
              placeholder="Enter Password"
              secureTextEntry={true}
              value={password || null}
              onChangeText={value => {
                setPassword(value);
              }}
            />
            <CustomButton
              disabled={loading}
              loading={loading}
              onPress={handleLogin}
              primary
              title="Login"
            />
          </View>
        </View>
      </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  scroll: {flexGrow: 1, backgroundColor: colors.white},
  container: {
    padding: 24,
    flex: 1,
    backgroundColor: colors.white,
  },
  title: {
    fontSize: 20,
    textAlign: 'center',
    paddingTop: 20,
    color: '#000',
    fontWeight: 'bold',
  },
  form: {
    paddingTop: 20,
  },
});
