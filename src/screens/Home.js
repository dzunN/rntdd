import React, {useState} from 'react';
import {Button, StyleSheet, Text, View} from 'react-native';

export default function Home({navigation}) {
  const [sapa, setSapa] = useState('');
  return (
    <View style={styles.container}>
      <Text style={styles.textSapa} testID="text-sapa">
        {sapa}
      </Text>

      <View style={styles.btn}>
        <Button
          title="Sapa user"
          testID="btn-sapa"
          onPress={() => setSapa('Halo Hindun')}
        />
      </View>
      <View style={styles.btn}>
        <Button
          title="Logout"
          color="red"
          onPress={() => navigation.navigate('Login')}
        />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  btn: {
    marginTop: 20,
  },
  textSapa: {
    marginTop: 20,
    fontSize: 24,
  },
});
