import axios from 'axios';

export const loginUser = (loginBody, navigation) => {
  try {
    const response = axios.post(
      'http://code.aldipee.com/api/v1/auth/login',
      loginBody,
    );
    // console.log(response.data);
    return response;
  } catch (error) {
    console.log(error);
  }
};
