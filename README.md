### Task 1 : 
- [x] Buat simple unit test dengan jest untuk fungsi tambah, kurang, kali,
- [x] Buat test snapshot sederhana untuk halaman home

### Task 2 :
- [ ] Lakukan unit test API Get
- [ ] Lakukan unit test navigasi dan behavior 1 halaman (login, register, atau dll)

### Task 3 :
- [ ] Login screen + API Login (book-app)
- [ ] Buat unit test, integration test, dan end to end test 